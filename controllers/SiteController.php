<?php

namespace app\controllers;

use app\models\Question;
use app\models\QuestionType;
use app\models\Test;
use Symfony\Component\Console\Helper\QuestionHelper;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    /**
     * @return int
     */
    public function actionStartTest(){
        $question_count = Yii::$app->request->get('question_count');
        $model = new Test([
            'datetime'=>date('Y-m-d H:i:s')
        ]);
        $model->save();
        $model->generateQuestions($question_count);
        return $model->id;
    }


    /**
     * Получаем инфо для рендера текста
     * @return null|string
     */
    public function actionGetQuestion(){
        $test_id = Yii::$app->request->get('test_id');
        $question = Question::find()->where([
            'test_id'=>$test_id,
            'pass_datetime'=>null
        ])->orderBy(['id'=>SORT_ASC])->one();
        if($question){
            return QuestionType::render(json_decode($question->params),$question->type_id);
        } else
            return null;
    }


    /**
     * Такие пироги
     * @return bool
     */
    public function actionSubmitQuestion(){
        $test_id = Yii::$app->request->get('test_id');
        $value = Yii::$app->request->get('value');
        $question = Question::find()->where([
            'test_id'=>$test_id,
            'pass_datetime'=>null
        ])->orderBy(['id'=>SORT_ASC])->one();

        if($question){
            /** @var $question Question */
            $question->pass_datetime = date('Y-m-d H:i:s');
            $question->answer = strval(round(floatval($value),2));

            $correct_answer = strval(QuestionType::calc(json_decode($question->params),$question->type_id));

            if($question->answer == $correct_answer){
                $question->is_correct = true;
            } else {
                $question->is_correct = false;
            }
            $question->save();
            return $question->is_correct;
        }
        return false;
    }
}
