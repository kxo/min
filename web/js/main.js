var current_test_id = null;
var correct_answers = null;
var current_answer_num = null;


function startTest(){ // initialize
    $.ajax({
        url: "/site/start-test",
        data:{
            question_count: $("#question_count").val()
        },
        success: function(test_id){
            current_test_id = test_id;
            current_answer_num = 0;
            correct_answers = 0;
            $("#start_screen").hide();
            $("#question_screen").show();
            showNextQuestion();
        }
    });
}

function submitQuestion(){
    var value = $("#input_answer").val();
    $.ajax({
        url: "/site/submit-question",
        data:{
            'test_id':current_test_id,
            'value':value
        },
        success: function(result){
            if(result){
                correct_answers++;
            } else{
                // =(
            }
            showNextQuestion();
        }
    });
}

function showNextQuestion(){
    $.ajax({
        url: "/site/get-question",
        data:{
            'test_id':current_test_id
        },
        success: function(result){
            if(result){
                current_answer_num++;
                $("#current_question_number").html(current_answer_num);
                $("#correct_question_count").html(correct_answers)
                $("#question_label").html(result);
                $("#input_answer").val('')

                // SHOW RESULT
                // CLEAR FORM WITH ANSWER
            } else{
                endTest();
            }
        }
    });
}

function endTest(){
    $("#question_screen").hide();
    $("#final_screen").show();
    $("#final_correct").html(correct_answers);
    $("#final_all").html(current_answer_num);

}