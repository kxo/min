<?php

use yii\db\Migration;

/**
 * Class m181130_181958_basic
 */
class m181130_181958_basic extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('test',[
            'id'=>$this->primaryKey(),
            'datetime'=>$this->timestamp()
        ]);

        $this->createTable('question',[
            'id'=>$this->primaryKey(),
            'test_id'=>$this->integer(),
            'params'=>$this->text(),
            'type_id'=>$this->integer(),
            'answer'=>$this->string(),
            'is_correct'=>$this->boolean(),
            'pass_datetime'=>$this->timestamp()
        ]);

        $this->addForeignKey('FK_question_test_id','question','test_id','test','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181130_181958_basic cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181130_181958_basic cannot be reverted.\n";

        return false;
    }
    */
}
