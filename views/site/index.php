<?php

/* @var $this yii\web\View */

$this->title = 'Онлайн Тестирование';
$this->registerJsFile('js/main.js',['depends' => [\yii\web\JqueryAsset::className()]])

?>
<div class="container">
    <div id="start_screen"
         style="padding-top: 25%;text-align: center">
        <span>Количество вопросов:</span><br>
        <input type="number"
               id = "question_count"
               value="10"
               placeholder="Количество вопросов"
               min="1" max="100"
               style="width: 157px;margin: 0 10px 10px 10px;"
        >
        <br>
        <a class="btn btn-info btn-raised" onclick="startTest()">
            Начать тестирование
        </a>
    </div>
    <div id="question_screen" class="text-center" style="display: none">
        <div style="padding: 16px;font-size: 14px;color: #00000094;">
            Вопрос №
            <span id="current_question_number" style="color: black;">8</span><br>
            ( правильных:
            <span id="correct_question_count" style="color: #007f00b5;">5</span>
            )
        </div>
        <br>
        <span id="question_label"> (12 + 48) * 12 = ?</span>
        <br><br>
        <input id="input_answer" title="Ваш ответ:" type="number" step="any" >
        <br><br>
        <a class="btn btn-success btn-raised" style="width: 175px;height: 30px;font-size: 12px;" onclick="submitQuestion()">
            Далее
        </a>
    </div>
    <div id="final_screen" style="text-align: center;display: none">
        <h2>Вы завершили тест!</h2>
        <h4>
            Ваш результаты <span id="final_correct">0</span>
            из <span id="final_all">10</span>
        </h4>
        <a class="btn btn-info btn-raised" onclick="location.reload()">
            Еще раз?
        </a>
    </div>

</div>
