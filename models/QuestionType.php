<?php
namespace app\models;

use Yii;

class QuestionType {

    public static $types = [
        0=>[
            'args'=>3,
        ],
        1=>[
            'args'=>3,
        ],
        2=>[
            'args'=>3,
        ],
        3=>[
            'args'=>2,
        ],
        4=>[
            'args'=>4,
        ]
    ];

    /**
     * Подсчет корректного итогового значения
     * @param $values
     * @param $type_id
     * @return float
     */
    public static function calc($values,$type_id){
        $value = 0;
        switch ($type_id){
            case 0:
                $value = ($values[0] + $values[1])*$values[2];
                break;
            case 1:
                $value = ($values[0] - $values[1])/$values[2];
                break;
            case 2:
                $value = (pow($values[0],2) + pow($values[1],2))/$values[2];
                break;
            case 3:
                $value = $values[0] / $values[1];
                break;
            case 4:
                $value = ($values[0] * $values[1])/($values[2] * $values[3]);
                break;
        }
        return round($value,2);
    }

    /**
     * Рендер как HTML
     * @param $values
     * @param $type_id
     * @return string
     */
    public static function render($values,$type_id){
        $str = '';
        switch ($type_id){
            case 0:
                $str = "( $values[0] + $values[1] ) * $values[2] = ?";
                break;
            case 1:
                $str = "( $values[0] - $values[1] ) / $values[2] = ?";
                break;
            case 2:
                $str = "( $values[0]^2 + $values[1]^2 ) / $values[2] = ?";
                break;
            case 3:
                $str = "$values[0] / $values[1] = ?";
                break;
            case 4:
                $str = "($values[0] * $values[1]) / ($values[2] * $values[3]) = ?";
                break;
        }
        return $str;
    }


    /**
     * @return Question
     */
    public static function generateQuestion(){
        $question_type_id = rand(0,count(self::$types)-1);
        $question_data = self::$types[$question_type_id];
        $values = [];
        for($i=0;$i<$question_data['args'];$i++){
            $values[] = rand(0,100) + (rand(0,100)/100);
        }

        $question = new Question([
            'params'=>json_encode($values),
            'type_id'=>$question_type_id,
        ]);
        return $question;
    }

}