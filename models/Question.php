<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property int $id
 * @property int $test_id
 * @property string $params
 * @property int $type_id
 * @property string $answer
 * @property bool $is_correct
 * @property string $pass_datetime
 *
 * @property Test $test
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['test_id', 'type_id'], 'default', 'value' => null],
            [['test_id', 'type_id'], 'integer'],
            [['params'], 'string'],
            [['is_correct'], 'boolean'],
            [['pass_datetime'], 'safe'],
            [['answer'], 'string', 'max' => 255],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Test::className(), 'targetAttribute' => ['test_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_id' => 'Test ID',
            'params' => 'Params',
            'type_id' => 'Type ID',
            'answer' => 'Answer',
            'is_correct' => 'Is Correct',
            'pass_datetime' => 'Pass Datetime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Test::className(), ['id' => 'test_id']);
    }
}
